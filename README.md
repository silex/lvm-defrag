LVM Defragmenter
================
Usage
-----
This lvm defragmenter is a simple program. It has the following command line options:

    Usage: lvmdefrag.rb [options]
        -i, --input FILE                 Input file with `pvdisplay -m` output
        -o, --output FILE                Output bash file with `pvmove` commands.
        -e, --execute                    Execute lvm commands
        -d, --device NAME                Device for which to execute, does nothing when an input file is given
        -h, --help                       Show this help

While it is possible to run this script directly, it is not recommended.
The recommended usage is as follows:

    sudo pvdisplay -m /dev/sdX > pvdisplay.out
    ./lvmdefrag.rb -i pvdisplay.out -o pvmoves.sh
    # Check yourself if the moves seem sane
    sudo sh pvmoves.sh

If you trust this script, you can do the above as follows:

    sudo ./lvmdefrag.rb -e -d /dev/sdX

Method
------
The script works by creating a mapping where physical extents are to where they should go.
It then moves them by repeating two actions, until the disk has been defragmented:

 1. Move physical extents to the free space where they belong.
 2. Move unmoved physical extents to free space where nothing belongs.

This should defragment an entire disk.
