require 'pp'
require 'optparse'

# Default option values
pvdisplay_file = false
bash_file = false
execute = false

# Parse options
options = OptionParser.new do |opts|
  opts.banner =
      "This program generates `pvmove` commands to defragment an LVM physical volume.\n\n"+
          "Usage: lvmdefrag.rb [options]"

  opts.on('-i','--input FILE','Input file with `pvdisplay -m` output') { |o| pvdisplay_file = o }
  opts.on('-o','--output FILE', 'Output bash file with `pvmove` commands. Default: bash.sh') { |o| bash_file = o }
  opts.on('-e','--execute', 'Execute lvm commands') { |o| execute = o }
  opts.on('-d','--device NAME', 'Device for which to execute, does nothing when an input file is given') { |o| execute = o }

  opts.on_tail('-h','--help', 'Show this help') do
    puts opts
    exit
  end
end
options.parse!

# Some validation
unless pvdisplay_file && bash_file || execute
  puts options
  exit
end

# Methods
# Convert array of values to array of ranges
# From Wayne Conrad, stackoverflow.com
def array_to_range(a)
  ranges = a.sort.uniq.inject([]) do |spans, n|
    if spans.empty? || spans.last.last != n - 1
      spans + [n..n]
    else
      spans[0..-2] + [spans.last.first..n]
    end
  end
  ranges
end

# Converts a hash with numeric keys/values to an array with ranges
def hash_to_range(h)
  ranges = Array.new
  range = nil
  h.each do |k,v|
    if !range.nil? && range.end == k-1 && h[k-1] == v-1
      range = (range.begin..k)
    else
      ranges << range if !range.nil?
      range = (k..k)
    end
  end
  ranges << range if range.is_a?(Range)
end

# Get pvdisplay output
if pvdisplay_file
  if File.exists?(pvdisplay_file)
    pvdisplay = File.open(pvdisplay_file)
  else
    puts "The file `#{pvdisplay_file}` does not exist"
  end
elsif execute
  if dev
    pvdisplay = `pvdisplay -m #{dev}`
  else
    puts "No device given"
  end
end

# Parse the pvdisplay output
structure = Array.new
lvs = Hash.new
dev = '/dev/sdX0'
pvdisplay.each_cons(3) do |pe,lv,le|
  # Detect the physical volume
  dev = pe[/(\/dev\/sd\w\d+)/,1] if pe.include? 'PV Name'

  # Detect a physical extent and parse this info
  if pe.include? 'Physical extent'
    # Get PE information
    pe = {
      :pe_start => pe[/(\d+) to (\d+)/,1].to_i,
      :pe_end   => pe[/(\d+) to (\d+)/,2].to_i,
      :vg       => lv[/\/dev\/([\w+.-]+)\//,1],
      :lv       => lv[/\/dev\/[\w+.-]+\/([\w+.-]+)/,1],
      :le_start => le[/(\d+) to (\d+)/,1].to_i,
      :le_end   => le[/(\d+) to (\d+)/,2].to_i,
    }

    # Calculate size
    pe[:size] = pe[:pe_end]-pe[:pe_start]+1

    # Nil is free
    pe[:lv] = 'FREE' if pe[:lv].nil?

    # Add to the structure
    structure << pe

    # Add the PEs to an LV
    if lvs.has_key?(pe[:lv])
      lvs[pe[:lv]][pe[:le_start]] = pe
    else
      lvs[pe[:lv]] = { pe[:le_start] => pe }
    end
  end
end

# Sort the LVs
lvs_order = lvs.keys.sort! do |a,b|
  case
    when a == 'FREE'
      1
    when b == 'FREE'
      -1
    when a[/([\w+.-]+)_(rmeta|rimage)/,1] == b[/([\w+.-]+)_(rmeta|rimage)/,1]
      -(a[/(rmeta|rimage)/,1] <=> b[/(rmeta|rimage)/,1])
    when a[/(rmeta|rimage)/,1] && !b[/(rmeta|rimage)/,1]
      -1
    when b[/(rmeta|rimage)/,1] && !a[/(rmeta|rimage)/,1]
      1
    else
      a <=> b
  end
end

# Create LV-map
pv_map = Hash.new
pv_free = Array.new
puts 'Target LV layout:'
pv_counter = 0

# Loop through LVs and add to map
lvs_order.each do |lvname|
  # Show LV name
  puts lvname

  # Loop through LEs
  lvs[lvname].sort.each do |_,pe|
    # Add all PEs to the map
    (pe[:pe_start]..pe[:pe_end]).each do |p|
      if lvname == 'FREE'
        pv_free << p
      else
        pv_map[p] = pv_counter
      end
      pv_counter += 1
    end

    # Output the new logical extent order
    puts "  Logical extents #{pe[:le_start]} to #{pe[:le_end]}" +
      " (Physical extents #{pv_map[pe[:pe_start]]} to #{pv_map[pe[:pe_end]]})" unless lvname == 'FREE'
  end

  # Report free PEs
  if lvname == 'FREE'
    puts "  Physical extent #{pv_counter - pv_free.size} to #{pv_counter - 1}"
  end
end

# Create an inverse PV map (to => from)
pv_mapi = pv_map.invert

# Generate the moves
puts "Generating pvmove commands"

# Store all move operations
pvmoves = Array.new

# Clear extents that need not move
pv_mapi.delete_if { |k,v| k == v}

# Generate pvmove commands
until pv_mapi.size == 0
  # Generate pvmoves by filling the free space
  while true
    pvmv = Hash.new

    # Generate the physical extends that should be moved to free space
    pv_free.delete_if do |p|
      # Add moveable extents to table
      unless pv_mapi[p].nil? || pv_mapi[p] == p
        # Move extents
        pvmv[pv_mapi[p]] = p
        # Remove extents from inverse table
        pv_mapi.delete(p)
        # Remove from free extents
        true
      else
        # Still free
        false
      end
    end

    # If there are no moves, stop the loop
    break if pvmv.size == 0

    # Mark free extents
    pv_free += pvmv.keys

    # Add to move operations
    pvmoves << pvmv
  end

  # Now move all unmoved blocks into free space
  while true
    # Sort the free physical extents
    pv_free.sort!

    # Create a hash for new moves
    pvmv = Hash.new

    # Save the old inverse map
    pv_mapi_old = pv_mapi

    # Loop through map
    pv_mapi_old.each do |to,from|
      # Move only if there is still free space
      if pv_free.size > 0
        # Generate add extents to moves
        pvmv[from] = pv_free.first

        # Mark the new movement
        pv_mapi[to] = pv_free.first
        pv_map[pv_free.first] = to

        # Remove the old movement
        #pv_mapi.delete(to)
        pv_map.delete(from)

        # Remove from free
        pv_free.shift
      else
        break
      end
    end

    # Add free space
    pv_free += pvmv.keys

    # Add moves
    pvmoves << pvmv
    break
  end
end

# Now generate the actual commands
commands = Array.new

# Statistics
pvmoves_sum = 0
#
pvmoves.each do |pvmvs|
  # Stats
  pvmoves_sum += pvmvs.size
  # Build command
  hash_to_range(pvmvs).each do |r|
    # Command base
    command = Hash.new
    command[:c] = "pvmove --alloc anywhere "
    # Ranges
    command[:c] += "#{dev}:#{r.begin}-#{r.end} #{dev}:#{pvmvs[r.begin]}-#{pvmvs[r.end]}"
    command[:d] = "#{r.end-r.begin+1} segments (#{(r.end-r.begin+1)*4/1024} GiB)"
    # Add to commands
    commands << command
  end unless pvmvs.empty?
end


if bash_file
  # Open the bash file for writing
  bf = open(bash_file, 'w')

  # Start of script
  bf.puts "#!/bin/bash"

  commands.each { |command| bf.puts command[:c] + " # " + command[:d] }

  # Add stats to script
  bf.puts "# Total: #{pvmoves_sum} segments (#{pvmoves_sum*4/1024} GiB)"

  # Tell where the commands are stored
  puts "Your pvmove commands can be found in `#{bash_file}`"

end

if execute
  commands.each do |command|
    puts "Executing: #{command[:c]}"
    puts `#{command[:c]}`
  end
end




